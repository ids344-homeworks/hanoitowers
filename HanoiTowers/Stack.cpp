// IDS344-01 - 2022-01 - Grupo #1 - Hanoi Towers
// 
// Nikita Kravchenko - 1101607
// Omar N��ez - 1101587
// Oliver Infante - 1100292
// Lervis Pinales - 1096323
// Daniel B�ez - 1073597

#include <iostream>
#include "HanoiTowers.h"
#include "GenericFunctions.h"
#include "StackStruct.h" 

using namespace std;

struct StackNode
{
    int data;
    StackNode* next;
};

StackNode* CreateStackNode(int data)
{
    StackNode* newNode = new StackNode();

    newNode->data = data;

    return newNode;
}

// se agrega un StackNode
void Push(StackNode*& stack, int data)
{
    StackNode* newNode = CreateStackNode(data);

    newNode->next = stack;

    stack = newNode;
}

// se elimina un StackNode
int Pop(StackNode*& stack)
{
    StackNode* aux = stack;
    int data = 0;

    if (aux != NULL)
    {
        data = aux->data;
        stack = aux->next;

        delete aux;
    }

    return data;
}

int ShowColumn(StackNode*& stack)
{
    int size = 0;
    StackNode* auxNode = stack;

    if (auxNode->next == NULL)
        return 0;

    while (auxNode != NULL)
    {
        size++;
        if (auxNode->next == NULL)
            break;

        ShowRing(auxNode->data);
        cout << endl;
        auxNode = auxNode->next;
    }

    return size;
}

// vaciar el stack
bool IsStackEmpty(StackNode*& stack)
{
    return (stack == NULL) ? true : false;
}

void ShowRing(int size)
{
    for (int i = 1; i <= size; i++)
        cout << "-";
}
