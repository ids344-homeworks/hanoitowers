// IDS344-01 - 2022-01 - Grupo #1 - Hanoi Towers
//
// Nikita Kravchenko - 1101607
// Omar N��ez - 1101587
// Oliver Infante - 1100292
// Lervis Pinales - 1096323
// Daniel B�ez - 1073597

#include <iostream>
#include "HanoiTowers.h"
#include "GenericFunctions.h"
#include "StackStruct.h" 

using namespace std;

const string programName = "Hanoi Towers";
int Repetitions = 0;

StackNode* tower1 = CreateStackNode(-1);
StackNode* tower2 = CreateStackNode(-1);
StackNode* tower3 = CreateStackNode(-1);

void SortRings(int ring, StackNode*& tower1, StackNode*& tower2, StackNode*& tower3)
{    
    if (ring == 0)
        return; 

    SortRings(ring - 1, tower1, tower3, tower2);

    Repetitions++;
    Push(tower3, Pop(tower1));
    ShowAllColumns();

    SortRings(ring - 1, tower2, tower1, tower3);
}

void FillTower(int amount, StackNode*& tower)//Llenar primera torre
{
    for (int i = amount; i > 0; i--)
        Push(tower, i);
}

void ShowAllColumns()
{
    ClearConsole(programName); // disable to unhide past movements

    cout << "Tower 1: " << endl;
    ShowColumn(tower1);

    cout << "Tower 2: " << endl;
    ShowColumn(tower2);

    cout << "Tower 3: " << endl;
    ShowColumn(tower3);

    cout << "Number of repetitions: " << Repetitions;

    Pause(); // turbo mode when disable
}

int main()
{
    int ringsNumber = -1;

    ClearConsole(programName);

    while (ringsNumber < 1) // sencilla comprobacion si numero sirve para algoritmo
    {
        cout << "\nNumber of rings: ";
        cin >> ringsNumber;
    }    

    FillTower(ringsNumber, tower1);

    ShowAllColumns();

    SortRings(ringsNumber, tower1, tower2, tower3);

    Pause();
}
