// IDS344-01 - 2022-01 - Grupo #1 - Hanoi Towers
//
// Nikita Kravchenko - 1101607
// Omar N��ez - 1101587
// Oliver Infante - 1100292
// Lervis Pinales - 1096323
// Daniel B�ez - 1073597

#include <iostream>
using namespace std;

struct StackNode;

StackNode* CreateStackNode(int = -1);
int ShowColumn(StackNode*&);

void Push(StackNode*&, int);
int Pop(StackNode*&);
bool IsStackEmpty(StackNode*&);
